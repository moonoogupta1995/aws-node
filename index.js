const express = require('express');
const app = express();

app.get('/',(req,res)=>{
    res.send("Hello World")
})


app.get('/getUsers',(req,res)=>{
    res.send([{
        "userName":"Raushan",
        "address":"Harsher"
    },{
        "userName":"Moonoo",
        "address":"Muzaffarpur"  
    }])
})

app.listen(3000,()=>{
    console.log("server is listening at 3000")
})